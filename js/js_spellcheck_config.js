(function($, Drupal, viewport, drupalSettings) {
  Drupal.behaviors.js_spellcheck_config= {
    attach: function (context) {      
      $('#edit-js-spellcheck-licence-version').on('change', function() {  
        if(this.value.toLowerCase() == 'trial')  {    
          $('#edit-js-spellcheck-licence-key').val('TRAIL');
        } else {
          $('#edit-js-spellcheck-licence-key').val('');

        }
      });
    }
  }
})(jQuery, Drupal, drupalSettings);
