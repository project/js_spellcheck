(function($, Drupal, viewport, drupalSettings) {
  Drupal.behaviors.js_spellcheck = {
    attach: function (context) {
      
      var lang_id = 'en';//drupalSettings.js_spellcheck.body_language;
      if (typeof(lang_id) == 'undefined') {
        lang_id = 'und';
      }      
      //Select all Text Fields except autocomplete Fields.
      $(".form-text:not('.form-autocomplete')").spellAsYouType({      
        theme: 'clean'
      }
);
      $('.text-full:not([data-drupal-selector="edit-body-0-value"]').spellAsYouType({      
        theme: 'clean'
      });
      //$('#edit-title-0-value,#edit-field-test-0-value').spellAsYouType(); 
      //$Spelling.SpellCheckInWindow('editors')
    }
  }

})(jQuery, Drupal, drupalSettings);
