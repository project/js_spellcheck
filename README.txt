Drupal 7 Version
Authored by Kevin Basarab (@kbasarab)
http://drupal.org/user/336254
http://kbasarab.com

Drupal 8 Version
Authored by Ramu Chidambaram(@ramuchidambaram)

Integrates automated spellchecking in a popup window or as you type using the third party
service http://www.javascriptspellcheck.com/.

## Installation
1. Download and install module.
2. Download library from http://www.javascriptspellcheck.com/.
3. Extract contents into DRUPAL_ROOT /libraries/js_spellcheck
4. Configure content types to enable spellchecking at: admin/config/spellcheck.

## Optional

**Install international dictionaries**
1. Download dictionary from
http://www.javascriptspellcheck.com/JavaScript_SpellChecking_Dictionaries
2. Extract .zip and place the .dic file into sites/all/libraries/js_spellcheck
3. Flush caches and choose new dictionary on admin/config/spellcheck
