<?php

namespace Drupal\js_spellcheck\Form;

use Drupal\node\Entity\NodeType;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class JsSpellCheckConfigForm.
 *
 * @package Drupal\js_spellcheck\Form
 */
class JsSpellCheckConfigForm extends ConfigFormBase {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Get Editable config names.
   *
   * @inheritDoc
   */
  protected function getEditableConfigNames() {
    return ['js_spellcheck.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'js_spellcheck_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('js_spellcheck.settings');
    $jsSpellChecksContentTypes = $config->get('js_spellcheck_content_types');
    $js_spellcheck_dictionary = $config->get('js_spellcheck_dictionary');
    $js_spellcheck_licence_version = $config->get('js_spellcheck_licence_version');
    $js_spellcheck_licence_key = $config->get('js_spellcheck_licence_key');

    $node_types = NodeType::loadMultiple();
    // If you need to display them in a drop down:
    $options = [];
    foreach ($node_types as $node_type) {
      $options[$node_type->id()] = $node_type->label();
    }
    // $js_spellcheck_licence_key = 'TRIAL';
    $form['licence_settings'] = [
      '#open' => TRUE,
      '#type' => 'details',
      '#title' => t('Licence settings'),
    ];
    $form['licence_settings']['js_spellcheck_licence_version'] = [
      '#type' => 'select',
      '#title' => $this->t('Licence Version'),
      '#options' => ['TRIAL' => 'TRIAL', 'LICENCED' => 'LICENCED'],
      '#default_value' => $js_spellcheck_licence_version,

    ];
    $form['licence_settings']['js_spellcheck_licence_key'] = [
      '#title' => t('Licence Key'),
      '#type' => 'textfield',
      '#default_value' => $js_spellcheck_licence_key,

    ];
    $form['js_spellcheck_content_types'] = [
      '#type' => 'checkboxes',
      '#title' => t('Content types to activate spellcheck on'),
      '#empty_option' => 'Select',
      '#options' => $options,
      '#default_value' => $jsSpellChecksContentTypes,
      '#multiple' => TRUE,
      '#attributes' => [
        'class' => ['restrict-content-type'],
      ],
    ];

    $dictionaries = $this->jsSpellCheckGetDictionaries();
    $form['js_spellcheck_dictionary'] = [
      '#title' => t('Dictionary to use'),
      '#type' => 'radios',
      '#default_value' => $js_spellcheck_dictionary,
      '#options' => $dictionaries,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Searches library filesystem for dictionaries.
   *
   * Dictionaries can be downloaded from:
   * http://www.javascriptspellcheck.com/JavaScript_SpellChecking_Dictionaries.
   */
  public function jsSpellCheckGetDictionaries() {
    $path = DRUPAL_ROOT . '/libraries/js_spellcheck';
    $files = file_scan_directory($path, '/.*\.dic$/');
    $dictionary = [];
    if (!is_array($files)) {
      return [];
    }
    foreach ($files as $file) {
      $dictionary[$file->name] = $file->name;
    }
    return $dictionary;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    if ($form_state->getValue('js_spellcheck_licence_key') && strtolower($form_state->getValue('js_spellcheck_licence_key')) != 'trial') {
      // We need to write in php default settings.
      $file = DRUPAL_ROOT . '/libraries/js_spellcheck/core/settings/' . "default-settings.php";

      // Read the entire string.
      $str = file_get_contents($file);
      // Replace something in the file string - this is a VERY simple example.
      $str = str_replace("TRIAL", $form_state->getValue('js_spellcheck_licence_key'), $str);
      // Write the entire string.
      file_put_contents($file, $str);

    }
    $this->config('js_spellcheck.settings')
      ->set('js_spellcheck_content_types', $form_state->getValue('js_spellcheck_content_types'))
      ->set('js_spellcheck_dictionary', $form_state->getValue('js_spellcheck_dictionary'))
      ->set('js_spellcheck_licence_version', $form_state->getValue('js_spellcheck_licence_version'))
      ->set('js_spellcheck_licence_key', $form_state->getValue('js_spellcheck_licence_key'))
      ->save();
  }

}
